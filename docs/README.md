[redis-json](README.md)

# redis-json

## Index

### Classes

* [RedisJson](classes/redisjson.md)

### Interfaces

* [IOptions](interfaces/ioptions.md)
* [IParser](interfaces/iparser.md)
* [ISetOptions](interfaces/isetoptions.md)
* [IStringifier](interfaces/istringifier.md)
