[redis-json](../README.md) › [RedisJson](redisjson.md)

# Class: RedisJson ‹**T**›

RedisJson eases the difficulties in storing JSON in redis.

 It stores the JSON in hashsets for simpler get and set of required
fields. It also allows you to override/set specific fields in
the JSON without rewriting the whole JSON tree. Which means that it
is literally possible to `Object.deepAssign()`.

 Everytime you store an object, RedisJson would store two hashsets
in Redis, one for data and the other for type information. This helps
during retrieval of data, to restore the type of data which was originally
provided. All these workaround are needed because Redis DOES NOT support
any other data type apart from String.

## Type parameters

▪ **T**

## Hierarchy

* **RedisJson**

## Implements

* IRedisJson‹T›

## Index

### Constructors

* [constructor](redisjson.md#constructor)

### Methods

* [clearAll](redisjson.md#clearall)
* [del](redisjson.md#del)
* [delT](redisjson.md#delt)
* [get](redisjson.md#get)
* [rewrite](redisjson.md#rewrite)
* [rewriteT](redisjson.md#rewritet)
* [set](redisjson.md#set)
* [setT](redisjson.md#sett)

## Constructors

###  constructor

\+ **new RedisJson**(`redisClient`: any, `options`: [IOptions](../interfaces/ioptions.md)): *[RedisJson](redisjson.md)*

Defined in src/lib/redis-json.ts:49

Intializes RedisJson instance

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`redisClient` | any | - | RedisClient instance(Preferred ioredis - cient).      It supports any redisClient instance that has      `'hmset' | 'hmget' | 'hgetall' | 'expire' | 'del' | 'keys'`      methods implemented |
`options` | [IOptions](../interfaces/ioptions.md) |  {} | Options for controlling the prefix  |

**Returns:** *[RedisJson](redisjson.md)*

## Methods

###  clearAll

▸ **clearAll**(): *Promise‹any›*

Defined in src/lib/redis-json.ts:201

Removes/deletes all the keys in the Cache,
having the prefix.

**Returns:** *Promise‹any›*

___

###  del

▸ **del**(`key`: string): *Promise‹any›*

Defined in src/lib/redis-json.ts:226

Removes the given key from Redis

Please use this method instead of
directly using `redis.del` as this method
ensures that even the corresponding type info
is removed. It also ensures that prefix is
added to key, ensuring no other key is
removed unintentionally

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`key` | string | Redis key  |

**Returns:** *Promise‹any›*

___

###  delT

▸ **delT**(`transaction`: Transaction, `key`: string): *Transaction*

Defined in src/lib/redis-json.ts:247

Removes the given key from Redis
using the given transaction

Please use this method instead of
directly using `redis.del` as this method
ensures that even the corresponding type info
is removed. It also ensures that prefix is
added to key, ensuring no other key is
removed unintentionally

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`transaction` | Transaction | Redis transaction |
`key` | string | Redis key  |

**Returns:** *Transaction*

___

###  get

▸ **get**(`key`: string, ...`fields`: string[]): *Promise‹Partial‹T› | undefined›*

Defined in src/lib/redis-json.ts:132

Retrieves the hashset from redis and
unflattens it back to the original Object

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`key` | string | Redis key |
`...fields` | string[] | List of fields to be retreived from redis.    This helps reduce network latency incase only a few fields are    needed.  |

**Returns:** *Promise‹Partial‹T› | undefined›*

request object from the cache

___

###  rewrite

▸ **rewrite**(`key`: string, `obj`: T, `options?`: [ISetOptions](../interfaces/isetoptions.md)): *Promise‹any›*

Defined in src/lib/redis-json.ts:179

Replace the entire hashset for the given key

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`key` | string | Redis key |
`obj` | T | JSON Object of type T |
`options?` | [ISetOptions](../interfaces/isetoptions.md) |   |

**Returns:** *Promise‹any›*

___

###  rewriteT

▸ **rewriteT**(`transaction`: Transaction, `key`: string, `obj`: T, `options?`: [ISetOptions](../interfaces/isetoptions.md)): *Transaction*

Defined in src/lib/redis-json.ts:192

Replace the entire hashset for the given key

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`transaction` | Transaction | Redis transaction |
`key` | string | Redis key |
`obj` | T | JSON Object of type T |
`options?` | [ISetOptions](../interfaces/isetoptions.md) |   |

**Returns:** *Transaction*

___

###  set

▸ **set**(`key`: string, `obj`: T, `options`: [ISetOptions](../interfaces/isetoptions.md)): *Promise‹any›*

Defined in src/lib/redis-json.ts:83

Flattens the given json object and
stores it in Redis hashset

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`key` | string | - | Redis key |
`obj` | T | - | JSON object to be stored |
`options` | [ISetOptions](../interfaces/isetoptions.md) |  {} |   |

**Returns:** *Promise‹any›*

___

###  setT

▸ **setT**(`transaction`: Transaction, `key`: string, `obj`: T, `options`: [ISetOptions](../interfaces/isetoptions.md)): *Transaction*

Defined in src/lib/redis-json.ts:108

Flattens the given json object and
stores it in Redis hashset using
the given transaction

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`transaction` | Transaction | - | redis transaction |
`key` | string | - | Redis key |
`obj` | T | - | JSON object to be stored |
`options` | [ISetOptions](../interfaces/isetoptions.md) |  {} |   |

**Returns:** *Transaction*
